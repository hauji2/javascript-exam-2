const getTheExactNumber = (numbers) => {
  // implement code here
  let filterResult = numbers.filter((item) => item % 3 === 0);
  // find the biggest number in the list
  return Math.max(...filterResult);

}

export default getTheExactNumber;