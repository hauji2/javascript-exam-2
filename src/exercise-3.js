const getNumberType = (number) => {
  // implement code here
  if (number <=0) return null;
  return number % 2 === 0 ? 'even number' : 'odd number';
};

export default getNumberType;
